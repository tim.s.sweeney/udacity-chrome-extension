(function () {
    // console.log(html2canvas)

    // console.log($(".video-stream.html5-main-video"))
    // html2canvas($(".video-stream.html5-main-video")).then(canvas => {
    //     document.body.appendChild(canvas)
    // })

    function copyToClipboard(str) {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    // just place a div at top right
    var tree = document.getElementById("tree-concepts")
    var string = ""
    for (var i = 0; i < tree.children[0].children.length; i++) {
        string += "### " + tree.children[0].children[i].children[0].title + "\n"
    }

    copyToClipboard(string)
    alert("Copied to clipboard: \n\n" + string)

})();